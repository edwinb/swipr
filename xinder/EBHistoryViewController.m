//
//  EBHistoryViewController.m
//  xinder
//
//  Created by edwin bosire on 31/08/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import "EBHistoryViewController.h"
#import "User.h"
#import "User+Extention.h"
#import "EBHistoryViewCell.h"

@interface EBHistoryViewController () <UICollectionViewDataSource, UICollectionViewDelegate>



@end

@implementation EBHistoryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.hidden = NO;
    self.title = @"History";

    [self.collectionView registerNib:[UINib nibWithNibName:@"EBHistoryViewCell" bundle:nil] forCellWithReuseIdentifier:@"EBHistoryViewCell"];
    
    [self fetchData];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated{
    
}

- (void)fetchData {
    
    self.historyItems = (NSMutableArray *)[User fetchAll];
    [self.collectionView reloadData];
}

- (NSMutableArray *)historyItems{
    
    if (!_historyItems) {
        _historyItems = [NSMutableArray new];
    }
    
    return _historyItems;
}

#pragma mark - Collectionview delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return [self.historyItems count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
 
    EBHistoryViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"EBHistoryViewCell" forIndexPath:indexPath];
    cell.user = [self.historyItems objectAtIndex:indexPath.row];
    
    return cell;
}

@end
