//
//  EBLoginViewController.m
//  xinder
//
//  Created by edwin bosire on 05/08/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import "EBLoginViewController.h"
#import "EBTinderClient.h"
#import "EBViewController.h"
#import "EBDataManager.h"
#import "User.h"
#import "User+Extention.h"
#import "EBTestLoginViewController.h"
#import "FUIButton.h"
#import "UIColor+FlatUI.h"
#import "UIFont+FlatUI.h"

@interface EBLoginViewController ()

@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (nonatomic) EBTinderClient *client;
@property (weak, nonatomic) IBOutlet FUIButton *facebookLoginButton;
@property (weak, nonatomic) IBOutlet UILabel *notificationMessage;
@property (weak, nonatomic) IBOutlet UIImageView *logo;
@property (nonatomic, strong) EBTestLoginViewController *loginViewController;

@end


@implementation EBLoginViewController

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.hidden = YES;
    
    NSString *message =[EBConfig shared].loginMessageBanner;
    
    if (message) {
        self.notificationMessage.text = message;
    }
    
    self.logo.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.logo.layer.borderWidth = 1.0f;
    self.logo.layer.cornerRadius = CGRectGetWidth(self.logo.frame) / 2;
    self.logo.layer.masksToBounds = YES;
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.logo.bounds];
    self.logo.layer.shadowColor = [UIColor blackColor].CGColor;
    self.logo.layer.shadowOffset = CGSizeMake(0.0f, 4.0f);
    self.logo.layer.shadowOpacity = 0.9f;
    self.logo.layer.shadowPath = shadowPath.CGPath;
    
    self.facebookLoginButton.buttonColor = [UIColor peterRiverColor];
    self.facebookLoginButton.shadowColor = [UIColor belizeHoleColor];
    self.facebookLoginButton.shadowHeight = 3.0f;
    self.facebookLoginButton.cornerRadius = 3.0f;
    self.facebookLoginButton.titleLabel.font = [UIFont boldFlatFontOfSize:16];
    [self.facebookLoginButton setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
    
    [self.facebookLoginButton setTitle:@"Facebook" forState:UIControlStateNormal];
    [self.facebookLoginButton addTarget:self action:@selector(presentFacebookLogin) forControlEvents:UIControlEventTouchUpInside];
    
    User *user = [User loggedInUser];
    
    if( user.facebookID && user.facebookToken){
        
        [self loginToTinder];
    }else {
        
        self.loginViewController = [[EBTestLoginViewController alloc] initWithNibName:nil bundle:nil];
    }
}

- (void)presentFacebookLogin {
    

    [self.navigationController pushViewController:self.loginViewController animated:YES];
    
    typeof (self) __weak weakself = self;

    [self.loginViewController setFacebookCompletion:^(BOOL success, NSString *token, NSString *facbeookID) {
        
        if (token) {
            
            [weakself getFacebookUserDetailsWithToken:token];
        }
        
//        [weakself.navigationController popToViewController:weakself animated:YES];
        [self.loginViewController dismissViewControllerAnimated:YES completion:nil];
    }];
    
}

- (void) loginToTinder {
    
    [[EBTinderClient sharedClient] authenticateWithTinderCompletion:^(BOOL success) {
        
        if (success){
            
            [self performSegueWithIdentifier:@"loginSuccessfulSegue" sender:self];
            
            [[EBDataManager shared] saveContext];
        }
    }];
}


- (void)getFacebookUserDetailsWithToken:(NSString *)token {
    
    NSString *action=@"me";
    
    NSString *url_string = [NSString stringWithFormat:@"https://graph.facebook.com/%@?", action];
    
    //tack on any get vars we have...
    
    NSDictionary *get_vars=nil;
    
    if ( (get_vars != nil) && ([get_vars count] > 0) ) {
        
        NSEnumerator *enumerator = [get_vars keyEnumerator];
        NSString *key;
        NSString *value;
        while ((key = (NSString *)[enumerator nextObject])) {
            
            value = (NSString *)[get_vars objectForKey:key];
            url_string = [NSString stringWithFormat:@"%@%@=%@&", url_string, key, value];
            
        }
    }
    
    url_string = [NSString stringWithFormat:@"%@access_token=%@", url_string, token];
    url_string = [url_string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"%@",url_string);
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url_string]];
    
    NSError *err;
    NSURLResponse *resp;
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:&resp error:&err];
    NSString *stringResponse = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
    NSLog(@"%@",stringResponse);
    NSError* error;
    NSDictionary *FBResResjson = [NSJSONSerialization
                                  JSONObjectWithData:response//1
                                  options:kNilOptions
                                  error:&error];
    NSLog(@"%@",FBResResjson);
    
    
    User *loggedInUser = [User loggedInUser];
    
    if (!loggedInUser) {
        
        loggedInUser = [User create];
    }
    
    loggedInUser.name = FBResResjson[@"first_name"];
    loggedInUser.facebookID = FBResResjson[@"id"];
    loggedInUser.facebookToken = token;
    loggedInUser.mainUser = @YES;
    [loggedInUser save];
    
    [self loginToTinder];
    
}

@end
