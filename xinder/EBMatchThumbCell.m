//
//  EBMatchThumbCell.m
//  xinder
//
//  Created by edwin bosire on 31/08/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import "EBMatchThumbCell.h"
#import "User+Extention.h"
#import "Photo.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "UIImage+ImageEffects.h"

@interface EBMatchThumbCell ()

@property (nonatomic, strong) UIImage *originalImage;
@end

@implementation EBMatchThumbCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib{
    [super awakeFromNib];
    
    self.layer.borderColor = [[UIColor cloudsColor] CGColor];
    self.layer.borderWidth = 3.0f;

}

- (void)setUser:(User *)user {
    
//    if (_user == user) {
//        return;
//    }
    _user = user;
    
    NSString *age = [NSString stringWithFormat:@"%@",(user.age.length > 0)? [NSString stringWithFormat:@"%@",user.age] : @" "];
    self.nameLabel.text = [NSString stringWithFormat:@"%@ %@", user.name, age];

    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"orderId" ascending:YES];
    NSArray *images = [[_user.photos allObjects] sortedArrayUsingDescriptors:@[descriptor]];
    Photo *profilePhoto = [images firstObject];
    NSString *altPicURL = profilePhoto.url;
    
    NSURL *url = [NSURL URLWithString:altPicURL];

    [self.profileImage sd_setImageWithURL:url
                         placeholderImage:[UIImage imageNamed:@"placeholder"]
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                    
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        self.profileImage.image = image;
                                    });
                                    
                                }];
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    
    if (selected) {
        self.layer.borderColor = [[UIColor alizarinColor] CGColor];
        self.likedIndicator.hidden = NO;
    }else {
        
        self.layer.borderColor = [[UIColor cloudsColor] CGColor];
        self.likedIndicator.hidden = YES;
    }
}
@end
