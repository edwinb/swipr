//
//  EBContainerViewController.m
//  xinder
//
//  Created by edwin bosire on 31/08/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import "EBContainerViewController.h"

//Child View Controllers

#import "EBLikedProfilesViewController.h"
#import "EBMatchedProfilesViewController.h"
#import "User+Extention.h"

#define ANIMATION_DURATION 0.3

typedef NS_OPTIONS(NSInteger, ViewSegmentationState) {
    ViewSegmentationStateMatches,
    ViewSegmentationStateLiked,
    ViewSegmentationStateNone, //This is a hack, to allow the view to be in werecommend state when it first loads
};

@interface EBContainerViewController ()


@property (weak, nonatomic) IBOutlet UIView *childView;
@property (strong, nonatomic) IBOutlet UIView *segmentationView;
@property (weak, nonatomic) IBOutlet UIButton *likedButton;
@property (weak, nonatomic) IBOutlet UIButton *matchesButton;


@property (nonatomic) EBLikedProfilesViewController *likedProfilesViewController;
@property (nonatomic) EBMatchedProfilesViewController *matchedProfileViewController;

@property (nonatomic) ViewSegmentationState segmentationState;

@end

@implementation EBContainerViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.segmentationState = ViewSegmentationStateLiked;
    self.title = @"History";
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    NSString *numberOfLikes = [NSString stringWithFormat:@"Matches (%lu)", (unsigned long)[[User fetchAllMatches ] count]];
    NSString *numberOfMatches = [NSString stringWithFormat:@"Passed (%lu)", (unsigned long)[[User fetchAllLiked] count]];

    [self.likedButton setTitle:numberOfLikes forState:UIControlStateNormal];
    [self.matchesButton setTitle:numberOfMatches forState:UIControlStateNormal];
    
//    self.segmentationState = ViewSegmentationStateLiked;
}

#pragma mark - Toggle views

- (IBAction)matchesSelected:(id)sender {
    self.segmentationState = ViewSegmentationStateMatches;
}

- (IBAction)likedSelected:(id)sender {
    self.segmentationState = ViewSegmentationStateLiked;
}

- (void)setSegmentationState:(ViewSegmentationState)segmentationState{
    
    if (_segmentationState == segmentationState) {
        return;
    }
    
    _segmentationState = segmentationState;
    
    switch (_segmentationState) {
        case ViewSegmentationStateMatches:{
            [UIView animateWithDuration:ANIMATION_DURATION
                             animations:^{
                                 self.likedButton.backgroundColor = [UIColor carrotColor];
                                 self.matchesButton.backgroundColor = [UIColor pumpkinColor];
                             }];
            
            [self toggleChildView:self.likedProfilesViewController withView:self.matchedProfileViewController];
            
            break;
        }
        case ViewSegmentationStateLiked:{
            [UIView animateWithDuration:ANIMATION_DURATION
                             animations:^{
                                 self.matchesButton.backgroundColor = [UIColor carrotColor];
                                 self.likedButton.backgroundColor = [UIColor pumpkinColor];
                             }];
            
            [self toggleChildView:self.matchedProfileViewController withView:self.likedProfilesViewController];
        }
            break;
            
        default:
            break;
    }
}

- (void)toggleChildView:(UIViewController *)fromViewController withView:(UIViewController *)toViewController{
    
    [fromViewController willMoveToParentViewController:nil];
    [self addChildViewController:toViewController];
    
    toViewController.view.alpha = 0.0f;
    [self.childView addSubview:toViewController.view];
    
    [UIView animateWithDuration:ANIMATION_DURATION
                     animations:^{
                         
                         toViewController.view.alpha = 1.0f;
                     } completion:^(BOOL finished) {
                         
                         [fromViewController.view removeFromSuperview];
                         [fromViewController removeFromParentViewController];
                         [fromViewController didMoveToParentViewController:self];
                     }];
    
}

#pragma mark - Properties

- (EBMatchedProfilesViewController *)matchedProfileViewController{
    
    if (!_matchedProfileViewController) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        _matchedProfileViewController = (EBMatchedProfilesViewController *)[storyBoard instantiateViewControllerWithIdentifier:NSStringFromClass([EBMatchedProfilesViewController class])];
    }
    return _matchedProfileViewController;
}

- (EBLikedProfilesViewController *)likedProfilesViewController{
    
    if (!_likedProfilesViewController) {
        
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        _likedProfilesViewController = (EBLikedProfilesViewController *)[storyBoard instantiateViewControllerWithIdentifier:NSStringFromClass([EBLikedProfilesViewController class])];
    }
    return _likedProfilesViewController;
}

@end
