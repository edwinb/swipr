//
//  EBLoginViewController.h
//  xinder
//
//  Created by edwin bosire on 05/08/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

@interface EBLoginViewController : UIViewController <FBLoginViewDelegate>

@end
