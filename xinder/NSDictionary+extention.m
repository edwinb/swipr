//
//  NSDictionary+extention.m
//  xinder
//
//  Created by edwin bosire on 31/08/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import "NSDictionary+extention.h"

@implementation NSDictionary (extention)

- (id)valueForKeyNull:(NSString *)key{
    
    id object = [self valueForKey:key];
    if (object) {
        return object;
    }else {
        return [NSNull null];
    }
}

@end
