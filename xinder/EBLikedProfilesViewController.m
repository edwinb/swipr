//
//  EBLikedProfilesViewController.m
//  xinder
//
//  Created by edwin bosire on 31/08/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import "EBLikedProfilesViewController.h"
#import "User+Extention.h"
@interface EBLikedProfilesViewController ()

@end

@implementation EBLikedProfilesViewController


- (void)viewDidLoad{
    [super viewDidLoad];
    
    self.title = @"Liked";
}

- (void)fetchData {
    
    self.historyItems = (NSMutableArray *)[User fetchAllMatches];
    [self.collectionView reloadData];
}

@end
