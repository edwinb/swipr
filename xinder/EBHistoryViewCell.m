//
//  EBHistoryViewCell.m
//  xinder
//
//  Created by edwin bosire on 31/08/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import "EBHistoryViewCell.h"
#import "User+Extention.h"
#import "User.h"
#import "Photo.h"
#import "SDWebImage/UIImageView+WebCache.h"


@implementation EBHistoryViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib{
    [super awakeFromNib];
    
//    self.profileImage.layer.borderColor = [[UIColor whiteColor] CGColor];
//    self.profileImage.layer.borderWidth = 5.0f;
    self.profileImage.layer.cornerRadius = CGRectGetWidth(self.profileImage.frame) / 2.0f ;
    
}

- (void)setUser:(User *)user {
    
    _user = user;
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"orderId" ascending:YES];
    NSArray *images = [[self.user.photos allObjects] sortedArrayUsingDescriptors:@[descriptor]];

    Photo *photo = [images firstObject];
    
    self.userName.text = _user.name;
    self.age.text = user.age;
    
    NSURL *url = [NSURL URLWithString:photo.url];
    
    [self.profileImage sd_setImageWithURL:url
            placeholderImage:[UIImage imageNamed:@"placeholder"]
                   completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                       
                       dispatch_async(dispatch_get_main_queue(), ^{
                           self.profileImage.image = image;
                       });
                       
                   }];
}



@end
