//
//  EBProfileViewController.m
//  xinder
//
//  Created by edwin bosire on 31/08/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import "EBProfileViewController.h"
#import "User+Extention.h"
#import "User.h"
#import "Photo.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "NSDate+NVTimeAgo.h"

@interface EBProfileViewController (){
    dispatch_queue_t backgroundQueue;

}

@property (weak, nonatomic) IBOutlet UIScrollView *headerScrollView;
@property (weak, nonatomic) IBOutlet UIScrollView *friendsListScrollView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastActive;
@property (weak, nonatomic) IBOutlet UILabel *bio;
@property (nonatomic) User *user;


@end

@implementation EBProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = NO;
    self.title = @"Profile";
    
    backgroundQueue = dispatch_queue_create("com.swipr", NULL);

    self.user = [User loggedInUser];
    
    NSString *age = [NSString stringWithFormat:@"%@",(self.user.age.length > 0)? [NSString stringWithFormat:@", %@",self.user.age] : @" "];
    self.nameLabel.text = [NSString stringWithFormat:@"%@%@", self.user.name, age];
    self.bio.text = self.user.bio;
    self.lastActive.text = [self.user.lastActive formattedAsTimeAgo];
    
    
    self.headerScrollView.layer.borderColor = [[UIColor midnightBlueColor] CGColor];
    self.headerScrollView.layer.borderWidth = 1.0f;
    self.headerScrollView.contentOffset = CGPointZero;
    
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"orderId" ascending:YES];
    NSArray *images = [[self.user.photos allObjects] sortedArrayUsingDescriptors:@[descriptor]];
    self.headerScrollView.contentSize = CGSizeMake(CGRectGetWidth(self.headerScrollView.frame)*images.count, self.headerScrollView.contentSize.height);
    self.headerScrollView.contentInset = UIEdgeInsetsMake(-70.0f, 0.0f, 0.0f, 0.0f);
    self.headerScrollView.alwaysBounceVertical = NO;
    
    for (int i =0; i < images.count; i++) {
        
        Photo *aPhoto = images[i];
        UIImageView *profileImage = [[UIImageView alloc] init];
        [self loadProfileImage:[NSURL URLWithString:aPhoto.url] forView:profileImage];
        profileImage.frame = CGRectMake(i*320, 0.0f, CGRectGetWidth(self.headerScrollView.frame), 320.0f);
        [self.headerScrollView addSubview:profileImage];
    }
}

- (void)loadProfileImage:(NSURL *)url forView:(UIImageView *)view{
  
    [view sd_setImageWithURL:url
            placeholderImage:[UIImage imageNamed:@"placeholder"]
                   completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                       
                       dispatch_async(dispatch_get_main_queue(), ^{
                           view.image = image;
                           view.contentMode = UIViewContentModeScaleAspectFit;
                       });

                   }];
  
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
