//
//  User+Extention.m
//  xinder
//
//  Created by edwin bosire on 31/08/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import "User+Extention.h"
#import "EBDataManager.h"

#define FACEBOOK_APP_TOKEN @"CAAGm0PX4ZCpsBAEe5koTJ8RZB1yAA1fTgKDH8hVNWyNwipp6suBdI9LQZCjPPcSv5fc9qyi92toncWq3W8Mcl7vUVhO3qZBhugIXyrZCJGe8C8xZAMaGY2Qfl3UxJPozY8QTL8Al0yJsKRbxIxmCYrD8ZCVazg2HbrNUZB2N0kVNDevbFH1cZB3uFE7DamniunJxfBUHCG3LbZBbKrT1b2odUk"
//CAAGm0PX4ZCpsBANEo8yUZBHxvTaLJbh8t1b7PO4Ye1OhqDC5wu9gxVMYrah6rZA2FvGFWkkwf0KyRhZA3gdA72PY0e8VPitnKXVIzkYYwRCz83BLdAuBO1ZCNUubFtj5jkYKrZCZA8UdC8FQKWZAzj7vdmSxHUw2eC3jJuxrh7yZBWhFxTlZACX6lTSzYAWJiAr2XPIuJEocFPqcGVW1JzUy3Et1U6VD7ypOcZD
@implementation User (Extention)
@dynamic age;

+ (User *)create{
   
    return [NSEntityDescription insertNewObjectForEntityForName:@"EBUser" inManagedObjectContext:[EBDataManager shared].managedObjectContext];
}

- (void)save{
    
    [[EBDataManager shared] saveContext];
}


+ (NSArray *)fetchAll {
    
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    request.returnsObjectsAsFaults = NO;
    
    NSManagedObjectContext* managedObjectContext = [EBDataManager shared].managedObjectContext;
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"EBUser"
                                                         inManagedObjectContext:managedObjectContext];
    [request setEntity:entityDescription];
    
    NSError* error;
    return [managedObjectContext executeFetchRequest:request error:&error];
}

+ (NSArray *)fetchAllMatches {
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"match == YES" ];
    return  [self executeRequestWithPredicate:predicate];
}

+ (NSArray *)fetchAllLiked {
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"liked == YES" ];
    return  [self executeRequestWithPredicate:predicate];
}

+ (NSArray *)fetchAllPassed {
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"liked == NO" ];
    return  [self executeRequestWithPredicate:predicate];
}

+ (void)deleteUser:(User *)user {
    
    NSManagedObjectContext* managedObjectContext = [EBDataManager shared].managedObjectContext;
    [managedObjectContext deleteObject:user];
    
    [managedObjectContext performBlockAndWait:^{
        NSError *saveError;
        if (![managedObjectContext save:&saveError]) {
            NSLog(@"Warning! %@", saveError.description);
        }
    }];
    
}

+ (User *)findUserForID:(NSString *)identification {
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"tinderID == %@", identification];
    User *user =  [[self executeRequestWithPredicate:predicate] firstObject];
    
    return user;
}

+ (User *)loggedInUser{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"mainUser == YES"];
    NSArray *results = [self executeRequestWithPredicate:predicate];
    User *user =  [results firstObject];
    
    return user;
}

#pragma mark - convenience method

+ (NSArray *)executeRequestWithPredicate:(NSPredicate *)predicate {
    
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    request.returnsObjectsAsFaults = NO;
    request.returnsDistinctResults = YES;
    request.resultType = NSManagedObjectResultType;
    
    NSManagedObjectContext* managedObjectContext = [EBDataManager shared].managedObjectContext;
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"EBUser"
                                                         inManagedObjectContext:managedObjectContext];
    [request setEntity:entityDescription];
    [request setPredicate:predicate];
    
    NSError* error;
    return [managedObjectContext executeFetchRequest:request error:&error];
}

+ (void)clearAuth {
    
    User * user = [User loggedInUser];
    user.facebookID = nil;
    user.facebookToken = nil;
    user.xAuthToken = nil;
    
    [user save];
    //Clear cookies as well.
    
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies]) {
        [storage deleteCookie:cookie];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
    

}
#pragma mark - property override

//- (NSString *)facebookToken {
////    return FACEBOOK_APP_TOKEN;
//}


- (NSString *)age {
    
    NSNumber *age = nil;
    if (self.dateOfBirth) {
        
        NSDate* now = [NSDate date];
        NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
                                           components:NSYearCalendarUnit
                                           fromDate:self.dateOfBirth
                                           toDate:now
                                           options:0];
        age = [NSNumber numberWithInt:[ageComponents year]];
    }
    
    return [NSString stringWithFormat:@"%@", ((age)? age : @"")];
}

@end
