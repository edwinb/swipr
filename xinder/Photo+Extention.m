//
//  Photo+Extention.m
//  xinder
//
//  Created by edwin bosire on 31/08/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import "Photo+Extention.h"
#import "EBDataManager.h"

@implementation Photo (Extention)


+ (Photo *)create{
    
    return [NSEntityDescription insertNewObjectForEntityForName:@"EBPhoto" inManagedObjectContext:[EBDataManager shared].managedObjectContext];
}

+ (void)save{
    
    [[EBDataManager shared] saveContext];
}
@end
