//
//  Photo.m
//  Swipr
//
//  Created by edwin bosire on 10/09/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import "Photo.h"
#import "User.h"


@implementation Photo

@dynamic height;
@dynamic url;
@dynamic width;
@dynamic orderId;
@dynamic user;

@end
