//
//  EBIAPItemCellView.h
//  xinder
//
//  Created by edwin bosire on 04/09/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FUIButton.h"

@interface EBIAPItemCellView : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *cellTitleLabel;
@property (weak, nonatomic) IBOutlet FUIButton *purchaseButton;

@end
