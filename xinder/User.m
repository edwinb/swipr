//
//  User.m
//  Swipr
//
//  Created by edwin bosire on 11/09/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import "User.h"
#import "EBFriends.h"
#import "EBLikes.h"
#import "Photo.h"


@implementation User

@dynamic ageFilterMax;
@dynamic ageFilterMin;
@dynamic bio;
@dynamic birthDay;
@dynamic commonFriends;
@dynamic commonFriendsCount;
@dynamic commonLikes;
@dynamic dateCreated;
@dynamic dateOfBirth;
@dynamic discoverable;
@dynamic distanceFilter;
@dynamic facebookID;
@dynamic facebookToken;
@dynamic gender;
@dynamic lastActive;
@dynamic liked;
@dynamic match;
@dynamic name;
@dynamic pass;
@dynamic tinderID;
@dynamic xAuthToken;
@dynamic mainUser;
@dynamic facebookFriends;
@dynamic likes;
@dynamic photos;

@end
