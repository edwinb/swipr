//
//  EBLikes.h
//  xinder
//
//  Created by edwin bosire on 31/08/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class User;

@interface EBLikes : NSManagedObject

@property (nonatomic, retain) NSNumber * facebookPageID;
@property (nonatomic, retain) NSSet *like;
@end

@interface EBLikes (CoreDataGeneratedAccessors)

- (void)addLikeObject:(User *)value;
- (void)removeLikeObject:(User *)value;
- (void)addLike:(NSSet *)values;
- (void)removeLike:(NSSet *)values;

@end
