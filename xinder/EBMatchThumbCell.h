//
//  EBMatchThumbCell.h
//  xinder
//
//  Created by edwin bosire on 31/08/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface EBMatchThumbCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *likedIndicator;


@property (nonatomic, strong) User *user;
@property (nonatomic) BOOL liked;


@end
