//
//  main.m
//  xinder
//
//  Created by edwin bosire on 27/07/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EBAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([EBAppDelegate class]));
    }
}
