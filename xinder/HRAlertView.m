//
//  HRAlertView.m
//  harrodsrewards
//
//  Created by Darren Norris on 08/07/2014.
//  Copyright (c) 2014 Darren Norris. All rights reserved.
//

#import "HRAlertView.h"
#import <objc/runtime.h>

@interface HRAlertWrapper : NSObject

@property (copy) void(^completionBlock)(UIAlertView *alertView, NSInteger buttonIndex);

@end

@implementation HRAlertWrapper

#pragma mark - UIAlertViewDelegate

- (id) initWithCompletionBlock:(void(^)(UIAlertView *alertView, NSInteger buttonIndex))completionBlock {
    self = [super init];
    if(self) {
        self.completionBlock = completionBlock;
    }
    return self;
}

// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (self.completionBlock) {
        self.completionBlock(alertView, buttonIndex);
    }
}

// Called when we cancel a view (eg. the user clicks the Home button). This is not called when the user clicks the cancel button.
// If not defined in the delegate, we simulate a click in the cancel button
- (void)alertViewCancel:(UIAlertView *)alertView {
    // Just simulate a cancel button click
    if (self.completionBlock) {
        self.completionBlock(alertView, alertView.cancelButtonIndex);
    }
}

@end


static const char kHRAlertWrapper;
@implementation HRAlertView

#pragma mark - Public

- (void)showWithCompletion:(void(^)(UIAlertView *alertView, NSInteger buttonIndex))completion {
    HRAlertWrapper *alertWrapper = [[HRAlertWrapper alloc] initWithCompletionBlock:completion];
    self.delegate = alertWrapper;
    
    // Set the wrapper as an associated object
    objc_setAssociatedObject(self, &kHRAlertWrapper, alertWrapper, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    // Show the alert as normal
    [self show];
}

@end
