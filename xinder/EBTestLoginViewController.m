//
//  EBTestLoginViewController.m
//  Swipr
//
//  Created by edwin bosire on 14/09/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import "EBTestLoginViewController.h"
#import "User+Extention.h"

@interface EBTestLoginViewController ()<UIWebViewDelegate>

@property(nonatomic,retain) UIWebView *webview;
@property (nonatomic, retain) NSString *accessToken;
@property (nonatomic, retain) NSString *facebookid;
@property(nonatomic,retain) UIActivityIndicatorView  *loadingIndicator;

@end

@implementation EBTestLoginViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = NO;
    [self loadFacebookLoginDialog];
    
    UIBarButtonItem *refresh = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refresh)];
    self.navigationItem.rightBarButtonItem = refresh;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.navigationController.navigationBar.hidden = NO;

    [User  clearAuth];
}

- (void)refresh {
    
     [User  clearAuth];
//    [self.webview removeFromSuperview];
    self.webview  = nil;
    [self loadFacebookLoginDialog];
    
}

- (void)loadFacebookLoginDialog{
    
    NSString   *facebookClientID =@"464891386855067";
    NSString   *redirectUri = @"http://www.facebook.com/connect/login_success.html";
    NSString  *extended_permissions=@"email";
    
    NSString *url_string = [NSString stringWithFormat:@"https://graph.facebook.com/oauth/authorize?client_id=%@&redirect_uri=%@&scope=%@&type=user_agent&display=touch", facebookClientID, redirectUri, extended_permissions];
    NSURL *url = [NSURL URLWithString:url_string];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    UIWebView *aWebView = [[UIWebView alloc] initWithFrame:self.view.bounds];
    [aWebView setDelegate:self];
    self.webview = aWebView;
    self.loadingIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.loadingIndicator.color=[UIColor darkGrayColor];
    self.loadingIndicator.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
    [self.loadingIndicator startAnimating];
    
    [self.webview loadRequest:request];
    [self.webview addSubview:self.loadingIndicator];
    [self.view addSubview:self.webview];
    
    
}

- (void)webViewDidFinishLoad:(UIWebView *)_webView {
    
    /**
     * Since there's some server side redirecting involved, this method/function will be called several times
     * we're only interested when we see a url like:  http://www.facebook.com/connect/login_success.html#access_token=..........
     */
    
    //get the url string
    [self.loadingIndicator stopAnimating];
    NSString *url_string = [((_webView.request).URL) absoluteString];
    
    //looking for "access_token="
    NSRange access_token_range = [url_string rangeOfString:@"access_token="];
    
    //looking for "error_reason=user_denied"
    NSRange cancel_range = [url_string rangeOfString:@"error_reason=user_denied"];
    
    //it exists?  coolio, we have a token, now let's parse it out....
    if (access_token_range.length > 0) {
        
        //we want everything after the 'access_token=' thus the position where it starts + it's length
        int from_index = access_token_range.location + access_token_range.length;
        NSString *access_token = [url_string substringFromIndex:from_index];
        
        //finally we have to url decode the access token
        access_token = [access_token stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        //remove everything '&' (inclusive) onward...
        NSRange period_range = [access_token rangeOfString:@"&"];
        
        //move beyond the .
        access_token = [access_token substringToIndex:period_range.location];
        
        //store our request token....
        self.accessToken = access_token;
        
        if (self.facebookCompletion) {
            self.facebookCompletion(NO, access_token, self.facebookid);
        }else {
            [self.navigationController popViewControllerAnimated:YES];
            [self.webview removeFromSuperview];
        }
        
        self.webview=nil;
        
        
    }
    else if (cancel_range.length > 0) {
        
        if (self.facebookCompletion) {
            self.facebookCompletion(NO, nil, nil);
        }else {
            [self.webview removeFromSuperview];
        }
        
        self.webview=nil;
        
        
    }
    
}



@end
