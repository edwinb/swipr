//
//  EBFriends.h
//  xinder
//
//  Created by edwin bosire on 31/08/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class User;

@interface EBFriends : NSManagedObject

@property (nonatomic, retain) NSNumber * facebookID;
@property (nonatomic, retain) NSSet *facebookFriends;
@end

@interface EBFriends (CoreDataGeneratedAccessors)

- (void)addFacebookFriendsObject:(User *)value;
- (void)removeFacebookFriendsObject:(User *)value;
- (void)addFacebookFriends:(NSSet *)values;
- (void)removeFacebookFriends:(NSSet *)values;

@end
