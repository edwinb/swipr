//
//  EBViewController.m
//  xinder
//
//  Created by edwin bosire on 27/07/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import "EBViewController.h"
#import "EBTinderClient.h"
#import "Photo.h"
#import "User.h"
#import "User+Extention.h"
#import "EBDataManager.h"
#import "UIImage+ImageEffects.h"
#import <QuartzCore/QuartzCore.h>
#import "UIColor+FlatUI.h"
#import "UIImage+ImageEffects.h"
#import "CHCircleGaugeView.h"
#import "EBSetting.h"
#import "EBSetting+Extention.h"
#import "FUIButton.h"
#import "UIColor+FlatUI.h"
#import "UIFont+FlatUI.h"
#import "UIImage+Utilities.h"
#define kBaseURL @"https://api.gotinder.com/"


@interface EBViewController (){
    NSInteger maxRecommendations;
    NSInteger totalRecommendations;
    BOOL stopSearch;
    CADisplayLink  *pulseTimer;
}

@property (weak, nonatomic) IBOutlet UILabel *matchesLabel;
@property (weak, nonatomic) IBOutlet UILabel *radarLabel;
@property (weak, nonatomic) IBOutlet UIView *raisedView;
@property (weak, nonatomic) IBOutlet UIButton *cherryPicker;
@property (weak, nonatomic) IBOutlet UIButton *profilePicture;
@property (weak, nonatomic) IBOutlet UIImageView *blurredProfilePicture;
@property (weak, nonatomic) IBOutlet UILabel *allLikes;
@property (weak, nonatomic) IBOutlet UILabel *allMatches;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UIView *profileImageBoundView;
@property (weak, nonatomic) IBOutlet UIButton *animatedStartButton;
@property (weak, nonatomic) IBOutlet CHCircleGaugeView *gaugeView;
@property (weak, nonatomic) IBOutlet FUIButton *resetMatchesButton;
@property (weak, nonatomic) IBOutlet FUIButton *likeSelectedButton;
@property (weak, nonatomic) IBOutlet UILabel *noMatchesLabel;
@property (weak, nonatomic) IBOutlet UIButton *settingsButton;
@property (weak, nonatomic) IBOutlet UIButton *historyButton;

@property (nonatomic) NSMutableURLRequest *request;
@property (nonatomic) NSMutableArray *likedUsers;
@property (nonatomic) __block NSMutableArray *matches;
@property (nonatomic) NSString *facebookToken;
@property (nonatomic) NSString *facebookID;
@property (nonatomic, strong) User *user;
@property (nonatomic) UIView *pulseView;

@end


#define kPULSE_SPEED 1.0f

@implementation EBViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.navigationController.navigationBar.hidden = YES;
    
    self.view.backgroundColor = [UIColor cloudsColor];
    self.matches = [NSMutableArray new];
    self.likedUsers = [NSMutableArray new];
    
    stopSearch = NO;
    self.matchesLabel.text = @"0";
    
    totalRecommendations = 0;
    
    self.profilePicture.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.profilePicture.layer.borderWidth = 5.0f;
    self.profilePicture.layer.cornerRadius = 5.0f;//CGRectGetWidth(self.profilePicture.frame) / 2.0f ;
    self.profilePicture.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5);
    
    {
        UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.profilePicture.bounds];
        self.profilePicture.layer.masksToBounds = NO;
        self.profilePicture.layer.shadowColor = [UIColor blackColor].CGColor;
        self.profilePicture.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
        self.profilePicture.layer.shadowOpacity = 0.9f;
        self.profilePicture.layer.shadowPath = shadowPath.CGPath;
    }
    
    [self.profilePicture setImage:[UIImage imageNamed:@"placeholder"] forState:UIControlStateNormal];
    self.profilePicture.titleLabel.textColor = [UIColor concreteColor];
    
    [self loadProfileImage];
    
    self.animatedStartButton.layer.masksToBounds = NO;
    
    self.gaugeView.center = [self.animatedStartButton convertPoint:self.animatedStartButton.center fromView:self.animatedStartButton.superview];
    
    self.gaugeView.trackTintColor = [UIColor carrotColor];
    self.gaugeView.gaugeTintColor = [UIColor pumpkinColor];
    self.gaugeView.textColor = [UIColor wetAsphaltColor];
    self.gaugeView.font = [UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:40.0f];
    self.gaugeView.value = 0.0f;
    self.gaugeView.trackWidth = 30.0f;
    self.gaugeView.gaugeWidth = 10.0f;
    self.gaugeView.gaugeStyle = CHCircleGaugeStyleOutside;
    self.gaugeView.state = CHCircleGaugeViewStateNA;
    self.gaugeView.notApplicableString = @"";
    self.gaugeView.unitsString = @"";
    self.gaugeView.valueTextLabel.hidden = YES;
    
    self.radarLabel.text = @"Search";
    
    self.resetMatchesButton.buttonColor = [UIColor carrotColor];
    self.resetMatchesButton.shadowColor = [UIColor pumpkinColor];
    self.resetMatchesButton.shadowHeight = 3.0f;
    self.resetMatchesButton.cornerRadius = 3.0f;
    self.resetMatchesButton.titleLabel.font = [UIFont boldFlatFontOfSize:16];
    [self.resetMatchesButton setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
    
    [self.resetMatchesButton setTitle:@"Reset" forState:UIControlStateNormal];
    [self.resetMatchesButton addTarget:self action:@selector(resetRecommendations:) forControlEvents:UIControlEventTouchUpInside];
    
    
    self.likeSelectedButton.buttonColor = [UIColor emerlandColor];
    self.likeSelectedButton.shadowColor = [UIColor nephritisColor];
    self.likeSelectedButton.shadowHeight = 3.0f;
    self.likeSelectedButton.cornerRadius = 3.0f;
    self.likeSelectedButton.titleLabel.font = [UIFont boldFlatFontOfSize:16];
    [self.likeSelectedButton setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
    
    [self.likeSelectedButton setTitle:@"Like Matches" forState:UIControlStateNormal];
    [self.likeSelectedButton addTarget:self action:@selector(likeSelectedMatches) forControlEvents:UIControlEventTouchUpInside];
    
    maxRecommendations = [EBSetting setting].batchValueFilter.integerValue;
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    self.navigationController.navigationBar.hidden = YES;
    self.userNameLabel.text = self.user.name;
    
    [self updateStats];
    
    if ([[EBTinderClient sharedClient] hasReset]) {
        [self resetRecommendations:nil];
    }
    
    if ( [EBTinderClient sharedClient].recommendations.count > 0) {
        self.resetMatchesButton.enabled = YES;
    }else {
        self.resetMatchesButton.enabled = NO;
    }
    
    [self startPulse];
    
    [self resetRecommendations:nil];
}

#pragma mark - prepare for segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"settingsSegue"]) {
        
    } else if ([segue.identifier isEqualToString:@"statsSegue"]){
        
    }
}

- (void)likeSelectedMatches {
    
    [self performSegueWithIdentifier:@"statsSegue" sender:self];
}

- (User *)user{
    
    return [User loggedInUser];
}


- (void)loadProfileImage {
    
    NSString *profilePicURL  = [NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?width=128&height=128", self.user.facebookID];
    NSURL *picURL = [NSURL URLWithString:profilePicURL];
    
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"orderId" ascending:YES];
    NSArray *images = [[self.user.photos allObjects] sortedArrayUsingDescriptors:@[descriptor]];
    Photo *profilePhoto = [images firstObject];
    NSString *altPicURL = profilePhoto.url;
    
    if (altPicURL.length > 0) {
        picURL = [NSURL URLWithString:altPicURL];
    }
    
    [self.profilePicture.imageView sd_setImageWithURL:picURL
                                     placeholderImage:[UIImage imageNamed:@"placeholder"]
                                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                
                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                    [[self.profilePicture imageView] setContentMode:UIViewContentModeScaleAspectFill];

                                                    [self.profilePicture setImage:image forState:UIControlStateNormal];
                                                });
                                                
                                            }];
}

- (UIView *)pulseView{
    
    if (!_pulseView) {
        
        _pulseView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 120.0f, 120.0f)];
        _pulseView.center = [self.gaugeView convertPoint:self.gaugeView.center fromView:self.gaugeView.superview];
        _pulseView.backgroundColor = [UIColor carrotColor];
        _pulseView.layer.cornerRadius = CGRectGetWidth(_pulseView.frame)/2;
        _pulseView.alpha = 0.7f;
        
        [self.gaugeView insertSubview:_pulseView belowSubview:self.radarLabel];
    }
    return _pulseView;
}

- (void)updateStats {
    
    NSUInteger likes = [[User fetchAllLiked] count];
    NSUInteger matches = [[User fetchAllMatches] count];
    
    self.allMatches.text = [NSString stringWithFormat:@"%lu", (unsigned long)matches];
    self.allLikes.text = [NSString stringWithFormat:@"%lu", (unsigned long)likes];
}


#pragma mark - Logout

- (IBAction)logout {
    
    [self performSegueWithIdentifier:@"loginPageSegue" sender:self];
}

#pragma mark - profile view controller

- (IBAction)openProfileViewController:(id )sender {
    
    [self performSegueWithIdentifier:@"profileSegue" sender:self];
}

- (IBAction)scaleProfilePicUp:(id)sender {
    
    [self scaleViewUp:self.profilePicture];
}

- (IBAction)scaleProfilePicDown:(id)sender {
    
    [self scaleViewDown:self.profilePicture];
}

- (IBAction)startSearch:(id)sender {
    
    [self startPulse];
    pulseTimer = nil;
    [pulseTimer invalidate];
//    
//    pulseTimer = [CADisplayLink displayLinkWithTarget:self selector:@selector(startPulse)];
//    pulseTimer.frameInterval = 50;
//    [pulseTimer addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
    
//    pulseTimer = [NSTimer scheduledTimerWithTimeInterval:kPULSE_SPEED+0.1
//                                                  target:self
//                                                selector:@selector(startPulse)
//                                                userInfo:nil
//                                                 repeats:YES];
//    [pulseTimer fire];
    stopSearch = NO;
    [self startAutoLiker:nil];
    
}

- (void)stopPulse {
    
    [pulseTimer invalidate];
    pulseTimer = nil;
    
//    if (!pulseTimer) {
//        [self startPulse];
//    }
    
}
- (IBAction)resetRecommendations:(id)sender {
    
    self.radarLabel.text = @"Search";
    
    maxRecommendations = [EBSetting setting].batchValueFilter.integerValue;
    [[EBTinderClient sharedClient].recommendations removeAllObjects];
    totalRecommendations = 0;
    
    [self.gaugeView setValue: 0 animated:NO];
    
    stopSearch = YES;
    [self stopPulse];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        //        stopSearch = NO;
    });
    
    self.likeSelectedButton.enabled = NO;
    self.resetMatchesButton.enabled = NO;
    [self matchesLabelShow:NO];
    NSLog(@"reset hit");
}

#pragma mark - auto liker

- (IBAction)startAutoLiker:(id)sender{
    
    [self matchesLabelShow:NO];
    if (totalRecommendations >= maxRecommendations || stopSearch){
        
        [self stopPulse];
        
        if (stopSearch) {
            
            totalRecommendations = [EBTinderClient sharedClient].recommendations.count;
            self.radarLabel.text = [NSString stringWithFormat:@"%@ \n matches found", (totalRecommendations == 0) ? @"No" : @(totalRecommendations)];
        }
        
        if (totalRecommendations > 0) {
            
            self.resetMatchesButton.enabled = YES;
            self.likeSelectedButton.enabled = YES;
        }
        return;
    }
    
    if (!self.user.facebookToken) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Unathorized"
                                                        message:@"Please login before proceeding"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    
    [[EBTinderClient sharedClient] recommendationsWithBlock:^(NSArray *recs, NSInteger count, NSError *connectionError) {
        
        if (stopSearch) {
            return;
        }
        self.resetMatchesButton.enabled = YES;
        self.likeSelectedButton.enabled = YES;
        
        totalRecommendations += count;
        if (totalRecommendations > 0 && !connectionError) {
            
            NSLog(@"received %lu recommendations", (unsigned long)count);
            
            [self.gaugeView setValue: (CGFloat)totalRecommendations / (CGFloat)maxRecommendations animated:YES];
            
            self.radarLabel.text = [NSString stringWithFormat:@"%li Found", (long)totalRecommendations];
            
            [self startAutoLiker:nil];
            
        }else if (connectionError) {
            
            [self stopPulse];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Problem"
                                                            message:@"Unable to find recommendations, please try again later."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil, nil];
            [alert show];
            
            [self matchesLabelShow:YES];
            return;
            
        }else{
            [self stopPulse];
            /*
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Matches"
             message:@"Looks like you've got no matches available at the moment. Try again later"
             delegate:nil
             cancelButtonTitle:@"OK"
             otherButtonTitles:nil, nil];
             [alert show];
             */
            [self matchesLabelShow:YES];
        }
        
    }];
    
    [self updateStats];
}

- (void)updateUI {
    
    self.matchesLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long)[EBTinderClient sharedClient].matches.count];
    //    self.likedLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long)[EBTinderClient sharedClient].likedUsers.count];
}


- (IBAction)scaleButtonUp:(id)sender {
    
    [self scaleViewUp:self.gaugeView];
}

- (IBAction)scaleButtonDown:(id)sender {
    
    [self scaleViewDown:self.gaugeView];
}

- (void)scaleViewUp:(UIView *)view {
    
    [UIView animateWithDuration:0.4f
                          delay:0.0f
         usingSpringWithDamping:0.3
          initialSpringVelocity:1.0f
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         view.transform = CGAffineTransformIdentity;
                     }
                     completion:nil];
}

- (void)scaleViewDown:(UIView *)view {
    
    [UIView animateWithDuration:0.4f
                          delay:0.0f
         usingSpringWithDamping:0.9
          initialSpringVelocity:1.0f
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         view.transform = CGAffineTransformScale(view.transform, 0.98, 0.98);
                     }
                     completion:nil];
}

- (void)startPulse {
    
    CABasicAnimation *scaleAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    scaleAnimation.duration = kPULSE_SPEED;;
    scaleAnimation.repeatCount = 1;
    scaleAnimation.fromValue = [NSNumber numberWithFloat:1.0f];
    scaleAnimation.toValue = [NSNumber numberWithFloat:5.8];
    
    CABasicAnimation *opacityAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    opacityAnimation.duration = kPULSE_SPEED;
    opacityAnimation.repeatCount = 1;
    opacityAnimation.fromValue = [NSNumber numberWithFloat:0.5];
    opacityAnimation.toValue = [NSNumber numberWithFloat:0.0];
    
    NSInteger random = arc4random() %10;
    NSString *opacityAnimationKey = [NSString stringWithFormat:@"animateOpacity%li", (long)random];
    NSString *scaleAnimationKey = [NSString stringWithFormat:@"animateScaley%li", (long)random];
    
    UIView *kamikazePulse = [NSKeyedUnarchiver unarchiveObjectWithData:[NSKeyedArchiver archivedDataWithRootObject:self.pulseView]];
    kamikazePulse.layer.cornerRadius = CGRectGetWidth(kamikazePulse.frame)/2;
    kamikazePulse.layer.borderColor = [UIColor alizarinColor].CGColor;
    kamikazePulse.layer.borderWidth = 1.0f;
    
    [kamikazePulse.layer addAnimation:opacityAnimation forKey:opacityAnimationKey];
    [kamikazePulse.layer addAnimation:scaleAnimation forKey:scaleAnimationKey];
    
    [self.gaugeView insertSubview:kamikazePulse belowSubview:self.gaugeView];
}

- (void)matchesLabelShow:(BOOL)hide {
    
    if (!hide) {
        
        [UIView animateWithDuration:0.3 animations:^{
            self.noMatchesLabel.alpha = 0.0f;
        } completion:^(BOOL finished) {
            self.noMatchesLabel.hidden = YES;
        }];
        
    }else {
        
        [UIView animateWithDuration:0.3 animations:^{
            self.noMatchesLabel.alpha = 1.0f;
        } completion:^(BOOL finished) {
            self.noMatchesLabel.hidden = NO;
        }];
    }
}
#pragma mark - UIAlertView

- (M13ProgressHUD *)progressHUD {
    
    if (!_progressHUD) {
        
        M13ProgressViewRing *ringView = [[M13ProgressViewRing alloc] init];
        ringView.showPercentage = NO;
        _progressHUD = [[M13ProgressHUD alloc] initWithProgressView:ringView];
        _progressHUD.progressViewSize = CGSizeMake(60.0, 60.0);
        _progressHUD.animationPoint = CGPointMake([UIScreen mainScreen].bounds.size.width / 2, [UIScreen mainScreen].bounds.size.height / 2);
        _progressHUD.indeterminate = YES;
        _progressHUD.maskColor = [UIColor colorWithWhite:0.1 alpha:0.5];
        _progressHUD.maskType = M13ProgressHUDMaskTypeGradient;
        _progressHUD.primaryColor = [UIColor alizarinColor];
        _progressHUD.secondaryColor = [UIColor pumpkinColor];
        _progressHUD.statusFont = [UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:12.0f];
        [self.view.window addSubview:_progressHUD];
    }
    
    return  _progressHUD;
}

- (void)dismissHUDWithSuccess:(BOOL)success {
    
    if (success) {
        [self.progressHUD performAction:M13ProgressViewActionSuccess animated:YES];
    }else{
        [self.progressHUD performAction:M13ProgressViewActionFailure animated:YES];
    }
    [self resetHUD];
    
}
- (void)showHUD {
    
    [self.progressHUD show:YES];
}
- (void)resetHUD {
    
    [super performSelector:@selector(resetHUDAfterDelay) withObject:nil afterDelay:1.5];
}

- (void)resetHUDAfterDelay {
    
    [self.progressHUD hide:YES];
    [self.progressHUD performAction:M13ProgressViewActionNone animated:NO];
}
- (void)setHUDStatusMessage:(NSString *)HUDStatusMessage{
    
    self.progressHUD.status = HUDStatusMessage;
}
@end
