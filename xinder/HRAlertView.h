//
//  HRAlertView.h
//  harrodsrewards
//
//  Created by Darren Norris on 08/07/2014.
//  Copyright (c) 2014 Darren Norris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HRAlertView : UIAlertView

- (void)showWithCompletion:(void(^)(UIAlertView *alertView, NSInteger buttonIndex))completion;

@end
