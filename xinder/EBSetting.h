//
//  EBSetting.h
//  xinder
//
//  Created by edwin bosire on 05/09/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface EBSetting : NSManagedObject

@property (nonatomic, retain) NSNumber * distanceFilter;
@property (nonatomic, retain) NSNumber * maxAgeFilter;
@property (nonatomic, retain) NSNumber * minAgeFilter;
@property (nonatomic, retain) NSNumber * recommendationLimit;
@property (nonatomic, retain) NSNumber * mojosBalance;
@property (nonatomic, retain) NSNumber * rateUsPerformed;
@property (nonatomic, retain) NSNumber * socialMediaSharing;
@property (nonatomic, retain) NSDate * lastActive;

@property (nonatomic) NSNumber *batchValueFilter;
@property (nonatomic) BOOL shouldReset;
@end
