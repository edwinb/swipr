//
//  EBBaseViewController.h
//  xinder
//
//  Created by edwin bosire on 31/08/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "M13ProgressHUD.h"
#import "M13ProgressViewRing.h"

@interface EBBaseViewController : UIViewController

@property (nonatomic) M13ProgressHUD *progressHUD;

- (void)dismissHUDWithSuccess:(BOOL)success;
- (void)showHUD;
- (void)resetHUD;
- (void)resetHUDAfterDelay;
- (void)setHUDStatusMessage:(NSString *)HUDStatusMessage;

@end
