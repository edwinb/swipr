//
//  EBSetting+Extention.m
//  xinder
//
//  Created by edwin bosire on 05/09/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import "EBSetting+Extention.h"
#import "EBDataManager.h"

@implementation EBSetting (Extention)


+ (EBSetting *)setting{
    
    EBSetting *currentSettings = [EBSetting fetch];
    
    if (!currentSettings) {
        
        currentSettings =  [NSEntityDescription insertNewObjectForEntityForName:@"EBSetting" inManagedObjectContext:[EBDataManager shared].managedObjectContext];
    }

    return currentSettings;
}

- (void)save{
    
      [[EBDataManager shared] saveContext];
}

+ (EBSetting *)fetch{
    
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    request.returnsObjectsAsFaults = NO;
    
    NSManagedObjectContext* managedObjectContext = [EBDataManager shared].managedObjectContext;
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"EBSetting"
                                                         inManagedObjectContext:managedObjectContext];
    [request setEntity:entityDescription];
    
    NSError* error;
    return [[managedObjectContext executeFetchRequest:request error:&error] firstObject];
}

@end
