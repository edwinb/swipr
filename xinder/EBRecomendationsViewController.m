//
//  EBStatsViewController.m
//  xinder
//
//  Created by edwin bosire on 31/08/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import "EBRecomendationsViewController.h"
#import "EBMatchThumbCell.h"
#import "User.h"
#import "User+Extention.h"
#import "EBTinderClient.h"
#import "EBDataManager.h"
#import "HRAlertView.h"
#import "UIImage+ImageEffects.h"

#define REUSE_IDENTIFIER @"EBMatchThumbCell"
@interface EBRecomendationsViewController () <UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic) NSMutableArray *recommendations;
@property (nonatomic) NSMutableArray *selectedRecommendations;
@property (weak, nonatomic) IBOutlet UIButton *likeAllButton;
@property (weak, nonatomic) IBOutlet UIView *toolBarView;

@end

@implementation EBRecomendationsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = NO;
    
    UIBarButtonItem *trashButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"delete"] style:UIBarButtonItemStylePlain target:self action:@selector(removeAllRecommendations)];
    self.navigationItem.rightBarButtonItem = trashButton;
    
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([EBMatchThumbCell class]) bundle:nil] forCellWithReuseIdentifier:REUSE_IDENTIFIER];
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.likeAllButton.bounds];
    self.likeAllButton.layer.masksToBounds = NO;
    self.likeAllButton.layer.shadowColor = [UIColor blackColor].CGColor;
    self.likeAllButton.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    self.likeAllButton.layer.shadowOpacity = 0.9f;
    self.likeAllButton.layer.shadowPath = shadowPath.CGPath;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchData) name:@"newRecommendations" object:nil];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    
    self.collectionView.allowsMultipleSelection = YES;
    [self fetchData];
    
}

- (void)fetchData{
    
    self.recommendations = [EBTinderClient sharedClient].recommendations;
    self.selectedRecommendations = [NSMutableArray arrayWithArray:self.recommendations];
    
    if (self.recommendations.count >0) {
        self.likeAllButton.enabled = YES;
        [self updateCountLabels];
    }else{
        self.title = @"No recommendations";
        self.likeAllButton.enabled = NO;
        [self.likeAllButton setTitle:@"Like" forState:UIControlStateNormal];
        
    }
    [self.collectionView reloadData];

}

- (void)updateCountLabels {
    
    self.title = [NSString stringWithFormat:@"Recommendations (%lu)", (unsigned long)self.selectedRecommendations.count];
    [self.likeAllButton setTitle:[NSString stringWithFormat:@"Like all (%lu)", (unsigned long)self.selectedRecommendations.count] forState:UIControlStateNormal];

}
- (void)removeAllRecommendations {
    
    HRAlertView *alerView = [[HRAlertView alloc] initWithTitle:@"Clear Recommendations"
                                                       message:@"Are you sure you want to delete your recommendations? "
                                                      delegate:self
                                             cancelButtonTitle:@"No"
                                             otherButtonTitles:@"Delete", nil];
    [alerView showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
        
        if (buttonIndex == 1) {
            [EBTinderClient sharedClient].recommendations = 0;
            [EBTinderClient sharedClient].likedUsers = nil;
            [EBTinderClient sharedClient].matches = nil;
            
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
    
 
    
    [self fetchData];
    
}
#pragma mark - Collectionview delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return [[EBTinderClient sharedClient].recommendations count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    EBMatchThumbCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:REUSE_IDENTIFIER forIndexPath:indexPath];
    cell.user = [[EBTinderClient sharedClient].recommendations objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.selectedRecommendations removeObject:[self.recommendations objectAtIndex:indexPath.row]];

    [self updateCountLabels];
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.selectedRecommendations addObject:[self.recommendations objectAtIndex:indexPath.row]];
     [self updateCountLabels];
}

#pragma mark - Like all Actions
- (IBAction)likeAllReccommendations:(id)sender {
    
    self.progressHUD.status = NSLocalizedString(@"Liking all recomendations", @"Updating personal details");
    [self showHUD];
    
    EBTinderClient *client = [EBTinderClient sharedClient];
    
    for (User *aReccomendation in self.recommendations) {
        
        if ([self.selectedRecommendations containsObject:aReccomendation]) {
            
            [client likeUser:aReccomendation onCompletion:^(BOOL success) {
                
                if (success) {
                    NSInteger row = [self.recommendations indexOfObject:aReccomendation];
                    EBMatchThumbCell *cell = (EBMatchThumbCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:row inSection:0]];
                    cell.liked = YES;
                    aReccomendation.liked = @YES;
                }else {
                    
                    aReccomendation.liked = @NO;
                }
                
                [aReccomendation save];
            }];

        }else {
            
            [client passUser:aReccomendation onCompletion:^(BOOL success) {
                
                NSLog(@"left swiped %@", aReccomendation.name);
            }];
        }
       
    }

    [EBTinderClient sharedClient].hasReset = YES;

    self.HUDStatusMessage = [NSString stringWithFormat:@"Liked all matches"];
    [self dismissHUDWithSuccess:YES];


    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:YES];

    });
}


@end
