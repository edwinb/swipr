//
//  Photo+Extention.h
//  xinder
//
//  Created by edwin bosire on 31/08/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import "Photo.h"

@interface Photo (Extention)

+ (Photo *)create;

+ (void)save;

@end
