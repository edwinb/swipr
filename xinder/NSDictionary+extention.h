//
//  NSDictionary+extention.h
//  xinder
//
//  Created by edwin bosire on 31/08/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (extention)

- (id)valueForKeyNull:(NSString *)key;

@end
