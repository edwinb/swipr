//
//  User.h
//  Swipr
//
//  Created by edwin bosire on 11/09/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class EBFriends, EBLikes, Photo;

@interface User : NSManagedObject

@property (nonatomic, retain) NSNumber * ageFilterMax;
@property (nonatomic, retain) NSNumber * ageFilterMin;
@property (nonatomic, retain) NSString * bio;
@property (nonatomic, retain) NSDate * birthDay;
@property (nonatomic, retain) NSString * commonFriends;
@property (nonatomic, retain) NSNumber * commonFriendsCount;
@property (nonatomic, retain) NSNumber * commonLikes;
@property (nonatomic, retain) NSDate * dateCreated;
@property (nonatomic, retain) NSDate * dateOfBirth;
@property (nonatomic, retain) NSNumber * discoverable;
@property (nonatomic, retain) NSNumber * distanceFilter;
@property (nonatomic, retain) NSString * facebookID;
@property (nonatomic, retain) NSString * facebookToken;
@property (nonatomic, retain) NSNumber * gender;
@property (nonatomic, retain) NSDate * lastActive;
@property (nonatomic, retain) NSNumber * liked;
@property (nonatomic, retain) NSNumber * match;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * pass;
@property (nonatomic, retain) NSString * tinderID;
@property (nonatomic, retain) NSString * xAuthToken;
@property (nonatomic, retain) NSNumber * mainUser;
@property (nonatomic, retain) NSSet *facebookFriends;
@property (nonatomic, retain) NSSet *likes;
@property (nonatomic, retain) NSSet *photos;
@end

@interface User (CoreDataGeneratedAccessors)

- (void)addFacebookFriendsObject:(EBFriends *)value;
- (void)removeFacebookFriendsObject:(EBFriends *)value;
- (void)addFacebookFriends:(NSSet *)values;
- (void)removeFacebookFriends:(NSSet *)values;

- (void)addLikesObject:(EBLikes *)value;
- (void)removeLikesObject:(EBLikes *)value;
- (void)addLikes:(NSSet *)values;
- (void)removeLikes:(NSSet *)values;

- (void)addPhotosObject:(Photo *)value;
- (void)removePhotosObject:(Photo *)value;
- (void)addPhotos:(NSSet *)values;
- (void)removePhotos:(NSSet *)values;

@end
