//
//  User+Extention.h
//  xinder
//
//  Created by edwin bosire on 31/08/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import "User.h"

@interface User (Extention)

+ (User *)create;

- (void)save;

+ (NSArray *)fetchAll;

+ (NSArray *)fetchAllMatches;

+ (NSArray *)fetchAllLiked;

+ (NSArray *)fetchAllPassed;

+ (void)deleteUser:(User *)user;

+ (User *)findUserForID:(NSString *)identification;

+ (User *)loggedInUser;

+ (void)clearAuth;

@property (nonatomic) NSString *age;

@end
