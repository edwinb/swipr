//
//  EBHistoryViewController.h
//  xinder
//
//  Created by edwin bosire on 31/08/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EBBaseViewController.h"

@interface EBHistoryViewController : EBBaseViewController

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic) NSMutableArray *historyItems;

- (void)fetchData;

@end
