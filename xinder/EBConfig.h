//
//  EBConfig.h
//  Swipr
//
//  Created by edwin bosire on 09/09/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EBConfig : NSObject

@property (nonatomic, strong) NSString *emailTitle;
@property (nonatomic, strong) NSString *emailBody;
@property (nonatomic, strong) NSArray *emailRecipients;
@property (nonatomic) BOOL isEmailHTML;

@property (nonatomic, strong) NSString *tweetPromoTag;
@property (nonatomic, strong) NSString *facebookPostTag;

@property (nonatomic, strong) NSString *rateUsURL;

@property (nonatomic, strong) NSString *loginMessageBanner;

+ (instancetype)shared;

- (void)loadConfig;

@end
