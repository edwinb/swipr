//
//  EBSettingsViewController.m
//  xinder
//
//  Created by edwin bosire on 31/08/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import "EBSettingsViewController.h"
#import "NMRangeSlider.h"
#import "User.h"
#import "User+Extention.h"
#import "FUIButton.h"
#import "UIColor+FlatUI.h"
#import "UIFont+FlatUI.h"
#import "UIBarButtonItem+FlatUI.h"
#import "EBSetting+Extention.h"
#import "EBSetting.h"
#import "EBTinderClient.h"
#import <FacebookSDK/FacebookSDK.h>
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>
#import "HRAlertView.h"

@interface EBSettingsViewController () <MFMailComposeViewControllerDelegate> {
    NSArray *sliderPositions;
    NSNumber *minAge;
    NSNumber *maxAge;
    NSNumber *distanceFilter;
}
@property (weak, nonatomic) IBOutlet UILabel *batchLabel;
@property (weak, nonatomic) IBOutlet UISlider *batchSlider;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UISlider *distanceSlider;
@property (weak, nonatomic) IBOutlet UILabel *ageLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *contentScrollView;
@property (weak, nonatomic) IBOutlet FBLoginView *facebookLogoutButton;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet FUIButton *feedbackButton;
@property (weak, nonatomic) IBOutlet FUIButton *rateusButton;

@property (weak, nonatomic) IBOutlet NMRangeSlider *ageSlider;

@end

@implementation EBSettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = NO;
    self.title = @"Settings";

    [UIBarButtonItem configureFlatButtonsWithColor:[UIColor midnightBlueColor]
                                  highlightedColor:[UIColor wetAsphaltColor]
                                      cornerRadius:3.0f
                                   whenContainedIn:[EBSettingsViewController class], nil];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(hideSettingsViewController:)];
     [self.navigationItem.rightBarButtonItem removeTitleShadow];
    
    sliderPositions = @[@20, @40, @60, @80, @100];
    
    self.batchSlider.maximumTrackTintColor = [UIColor concreteColor];
    self.batchSlider.minimumTrackTintColor = [UIColor wetAsphaltColor];
    self.batchSlider.maximumValue = sliderPositions.count - 1;
    self.batchSlider.minimumValue = 0;
    
    self.contentScrollView.contentSize = CGSizeMake(320.0f, CGRectGetMaxY(self.view.frame)+300.0f);
    
    self.closeButton.layer.borderColor = [[UIColor cloudsColor] CGColor];
    self.closeButton.layer.borderWidth = 2.0f;
    self.closeButton.layer.cornerRadius = CGRectGetWidth(self.closeButton.frame)/2;

    self.feedbackButton.buttonColor = [UIColor carrotColor];
    self.feedbackButton.shadowColor = [UIColor pumpkinColor];
    self.feedbackButton.shadowHeight = 3.0f;
    self.feedbackButton.cornerRadius = 3.0f;
    self.feedbackButton.titleLabel.font = [UIFont boldFlatFontOfSize:16];
    [self.feedbackButton setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];

    self.rateusButton.buttonColor = [UIColor carrotColor];
    self.rateusButton.shadowColor = [UIColor pumpkinColor];
    self.rateusButton.shadowHeight = 3.0f;
    self.rateusButton.cornerRadius = 3.0f;
    self.rateusButton.titleLabel.font = [UIFont boldFlatFontOfSize:16];
    [self.rateusButton setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
    
    [self configureSliders];

    CGFloat batch  =[EBSetting setting].recommendationLimit.floatValue;
    
    [self.batchSlider setValue:batch animated:YES];
    [self setBatchLabelTextWithValue:batch];
    
    if ([MFMailComposeViewController canSendMail]){
        self.feedbackButton.enabled = YES;
    }else {
        self.feedbackButton.enabled = NO;
    }
    
    [[EBConfig shared] loadConfig];
}

- (void)hideSettingsViewController:(id)sender {
    
    [[EBSetting setting] save];
    [[User loggedInUser] save];
    
    NSLog(@"updating user settings");
    [[EBTinderClient sharedClient] updateProfile:^(BOOL success) {
        NSLog(@"user settings updated success %d", success);
        if (success) {
            
//            [EBSetting setting].shouldReset = YES;
        }
        [[EBTinderClient sharedClient] resetRecommendations];
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];

    }];
}

#pragma mark - Age  Slider

- (void) configureSliders{
    
    User *currentUser = [User loggedInUser];
    
    // Age Slider
    
    self.ageSlider.minimumValue = 0.18f;
    self.ageSlider.maximumValue = 0.50;
    
    self.ageSlider.lowerValue = currentUser.ageFilterMin.floatValue/100;
    self.ageSlider.upperValue = currentUser.ageFilterMax.floatValue/100;
    
    self.ageSlider.minimumRange = 0.04f;
    
    minAge = [NSNumber numberWithInteger:self.ageSlider.lowerValue*100];
    maxAge = [NSNumber numberWithInteger:self.ageSlider.upperValue*100];
    
    self.ageLabel.text = [NSString stringWithFormat:@"%@-%@", minAge, maxAge];

    // Distance
    
    [self.distanceSlider setValue:currentUser.distanceFilter.floatValue animated:YES];
    self.distanceLabel.text = [NSString stringWithFormat:@"%.0f km", currentUser.distanceFilter.floatValue];

}



- (IBAction)batchValueSelected:(id)sender {
    
    UISlider *slider = (UISlider *)sender;
 
    [self setBatchLabelTextWithValue:slider.value];
    
}

- (void)setBatchLabelTextWithValue:(CGFloat)sliderValue {
    
    NSUInteger index = (NSUInteger)(sliderValue + 0.5);
    NSNumber *number = sliderPositions[index];
    NSLog(@"sliderIndex: %i", (int)index);
    NSLog(@"number: %@", number);
    NSInteger value = 0;
    switch (number.integerValue) {
        case 20:
            value = 50;
            break;
        case 40:
            value = 250;
            break;
        case 60:
            value = 500;
            break;
        case 80:
            value = 1000;
            break;
        case 100:
            value = 3000;
            
            break;
            
        default:
            break;
    }
    self.batchLabel.text = [NSString stringWithFormat:@"%li", (long)value];
}

- (IBAction)batchValue:(id)sender {
    
    UISlider *slider = (UISlider *)sender;

    NSUInteger index = (NSUInteger)(slider.value + 0.5);
    [slider setValue:index animated:YES];

    [EBSetting setting].recommendationLimit = [NSNumber numberWithInteger:index];
}

- (IBAction)distanceValueChanged:(id)sender {
    
    self.distanceLabel.text = [NSString stringWithFormat:@"%.0f km", ((UISlider *)sender).value];
    
    distanceFilter = [NSNumber numberWithInteger:((UISlider *)sender).value];
    [User loggedInUser].distanceFilter = distanceFilter;

}

- (IBAction)ageValueChanged:(id)sender {
    
    NMRangeSlider *slider = ((NMRangeSlider *)sender);
    self.ageLabel.text = [NSString stringWithFormat:@"%.0f-%.0f",slider.lowerValue*100, slider.upperValue*100];
    
    minAge = [NSNumber numberWithInteger:self.ageSlider.lowerValue*100];
    maxAge = [NSNumber numberWithInteger:self.ageSlider.upperValue*100];
    
     [User loggedInUser].ageFilterMax = maxAge;
     [User loggedInUser].ageFilterMin = minAge;

}

#pragma mark - send feedback.

- (IBAction)sendFeedBack:(id)sender {
    
    NSString *emailTitle = [EBConfig shared].emailTitle;
    NSString *messageBody = [EBConfig shared].emailBody;
    NSArray *toRecipents = [EBConfig shared].emailRecipients;
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
}

-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self dismissViewControllerAnimated:YES completion:^{
        [self hideSettingsViewController:nil];
    }];
    
}

- (IBAction)rateUsOnItunes:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[EBConfig shared].rateUsURL]];
}

#pragma mark - social media 

- (IBAction)promoteTweet:(id)sender {
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]){
        SLComposeViewController *tweetSheet = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet setInitialText:[EBConfig shared].tweetPromoTag];
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }
}

- (IBAction)promoteWithFacebook:(id)sender {
}


#pragma mark - logout

- (IBAction)fbLogout:(id)sender {
    
    HRAlertView *alert = [[HRAlertView alloc] initWithTitle:@"Logout"
                                                    message:@"Are you sure you want to logout? You will need to login with your facebook credentials to continue using the app"
                                                   delegate:nil
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"Logout", nil];
    [alert showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
        
        if (buttonIndex == 0) {
            NSLog(@"cancel pressed");
            
        }else if (buttonIndex ==1){
            NSLog(@"logout pressed");
            
            [User clearAuth];
            
            [self performSegueWithIdentifier:@"logoutSegue" sender:self];

        }

    }];
}
@end
