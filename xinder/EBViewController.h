//
//  EBViewController.h
//  xinder
//
//  Created by edwin bosire on 27/07/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "M13ProgressHUD.h"
#import "M13ProgressViewRing.h"

@interface EBViewController : UIViewController

@property (nonatomic) M13ProgressHUD *progressHUD;


- (IBAction)resetRecommendations:(id)sender;

@end
