//
//  EBBaseViewController.m
//  xinder
//
//  Created by edwin bosire on 31/08/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import "EBBaseViewController.h"
#import "UIColor+FlatUI.h"

@interface EBBaseViewController ()

@end

@implementation EBBaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor carrotColor], NSForegroundColorAttributeName,nil]];
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.tintColor = [UIColor cloudsColor];
    self.navigationController.navigationBar.barTintColor = [UIColor midnightBlueColor];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    self.navigationController.navigationBar.hidden = YES;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIAlertView

- (M13ProgressHUD *)progressHUD {
    
    if (!_progressHUD) {
        
        M13ProgressViewRing *ringView = [[M13ProgressViewRing alloc] init];
        ringView.showPercentage = NO;
        _progressHUD = [[M13ProgressHUD alloc] initWithProgressView:ringView];
        _progressHUD.progressViewSize = CGSizeMake(60.0, 60.0);
        _progressHUD.animationPoint = CGPointMake([UIScreen mainScreen].bounds.size.width / 2, [UIScreen mainScreen].bounds.size.height / 2);
        _progressHUD.indeterminate = YES;
        _progressHUD.maskColor = [UIColor colorWithWhite:0.1 alpha:0.5];
        _progressHUD.maskType = M13ProgressHUDMaskTypeGradient;
        _progressHUD.primaryColor = [UIColor alizarinColor];
        _progressHUD.secondaryColor = [UIColor pumpkinColor];
        _progressHUD.statusFont = [UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:12.0f];
        [self.view.window addSubview:_progressHUD];
    }
    
    return  _progressHUD;
}

- (void)dismissHUDWithSuccess:(BOOL)success {
    
    if (success) {
        [self.progressHUD performAction:M13ProgressViewActionSuccess animated:YES];
    }else{
        [self.progressHUD performAction:M13ProgressViewActionFailure animated:YES];
    }
    [self resetHUD];
    
}
- (void)showHUD {
    
    [self.progressHUD show:YES];
}
- (void)resetHUD {
    
    [super performSelector:@selector(resetHUDAfterDelay) withObject:nil afterDelay:1.5];
}

- (void)resetHUDAfterDelay {
    
    [self.progressHUD hide:YES];
    [self.progressHUD performAction:M13ProgressViewActionNone animated:NO];
}
- (void)setHUDStatusMessage:(NSString *)HUDStatusMessage{
    
    self.progressHUD.status = HUDStatusMessage;
}

@end
