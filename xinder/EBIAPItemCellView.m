//
//  EBIAPItemCellView.m
//  xinder
//
//  Created by edwin bosire on 04/09/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import "EBIAPItemCellView.h"
#import "UIColor+FlatUI.h"
#import "UIFont+FlatUI.h"

@implementation EBIAPItemCellView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void)awakeFromNib{
    
    [super awakeFromNib];
    
    self.purchaseButton.buttonColor = [UIColor carrotColor];
    self.purchaseButton.shadowColor = [UIColor pumpkinColor];
    self.purchaseButton.shadowHeight = 3.0f;
    self.purchaseButton.cornerRadius = 3.0f;
    self.purchaseButton.titleLabel.font = [UIFont boldFlatFontOfSize:16];
    [self.purchaseButton setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];

}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
