//
//  EBConfig.m
//  Swipr
//
//  Created by edwin bosire on 09/09/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import "EBConfig.h"

@implementation EBConfig


+  (instancetype)shared{
    
    static id shared = nil;
    if (!shared){
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            shared = [[self alloc] init];
        });
    }
    
    return shared;
}


- (instancetype)init
{
    self = [super init];
    if (self) {
        [self loadConfig];
          }
    return self;
}

- (void)loadConfig {
    
    [PFConfig getConfigInBackgroundWithBlock:^(PFConfig *config, NSError *error) {
        
        if (error) {
            config = [PFConfig currentConfig];
        }
        
        self.emailTitle = config[@"emailTitle"];
        self.emailBody  = config[@"emailBody"];
        self.emailRecipients  = config[@"emailRecipients"];
        self.isEmailHTML  = (BOOL)config[@"isEmailHTML"];
        self.tweetPromoTag  = config[@"tweetPromoTag"];
        self.facebookPostTag  = config[@"facebookPostTag"];
        self.rateUsURL = config[@"rateUs"];
        self.loginMessageBanner = config[@"loginMessageBanner"];
    }];

}


@end
