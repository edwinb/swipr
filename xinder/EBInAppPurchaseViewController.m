//
//  EBInAppPurchaseViewController.m
//  xinder
//
//  Created by edwin bosire on 04/09/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import "EBInAppPurchaseViewController.h"
#import "EBIAPItemCellView.h"


@interface EBInAppPurchaseViewController () <UICollectionViewDataSource, UICollectionViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation EBInAppPurchaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"EBIAPItemCellView" bundle:nil] forCellWithReuseIdentifier:@"EBIAPItemCellView"];
    
    self.closeButton.layer.borderColor = [[UIColor cloudsColor] CGColor];
    self.closeButton.layer.borderWidth = 2.0f;
    self.closeButton.layer.cornerRadius = CGRectGetWidth(self.closeButton.frame)/2;
}
- (IBAction)dismissSelf:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Collectionview delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return 10;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    EBIAPItemCellView *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"EBIAPItemCellView" forIndexPath:indexPath];
    
    return cell;
}
@end
