//
//  EBTestLoginViewController.h
//  Swipr
//
//  Created by edwin bosire on 14/09/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EBBaseViewController.h"

typedef void(^FacebookLoginBlock)(BOOL success, NSString *token, NSString *facbeookID);

@interface EBTestLoginViewController : EBBaseViewController

@property (nonatomic, strong) FacebookLoginBlock facebookCompletion;

@end
