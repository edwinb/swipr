//
//  EBSetting+Extention.h
//  xinder
//
//  Created by edwin bosire on 05/09/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import "EBSetting.h"

@interface EBSetting (Extention)

+ (EBSetting *)setting;

- (void)save;

+ (EBSetting *)fetch;

@end
