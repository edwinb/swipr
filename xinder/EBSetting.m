//
//  EBSetting.m
//  xinder
//
//  Created by edwin bosire on 05/09/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import "EBSetting.h"


@implementation EBSetting

@dynamic distanceFilter;
@dynamic maxAgeFilter;
@dynamic minAgeFilter;
@dynamic recommendationLimit;
@dynamic mojosBalance;
@dynamic rateUsPerformed;
@dynamic socialMediaSharing;
@dynamic lastActive;


- (NSNumber *)batchValueFilter{

    NSArray *sliderPositions = @[@20, @40, @60, @80, @100];
    NSNumber *number = sliderPositions[self.recommendationLimit.integerValue];
  
    switch (number.integerValue) {
        case 20:
            return @50;
            break;
        case 40:
            return @250;
            break;
        case 60:
            return @500;
            break;
        case 80:
            return @1000;
            break;
        case 100:
            return @3000;
            break;
    
    }
    return @250;
}
@end
