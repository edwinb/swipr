//
//  EBMatchedProfilesViewController.m
//  xinder
//
//  Created by edwin bosire on 31/08/2014.
//  Copyright (c) 2014 Edwin Bosire. All rights reserved.
//

#import "EBMatchedProfilesViewController.h"
#import "User+Extention.h"
@interface EBMatchedProfilesViewController ()

@end

@implementation EBMatchedProfilesViewController


- (void)viewDidLoad{
    [super viewDidLoad];
    
    self.title = @"Matches";
}

- (void)fetchData {
    
    self.historyItems = (NSMutableArray *)[User fetchAllLiked];
    [self.collectionView reloadData];
}

@end
